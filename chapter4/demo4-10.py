from atexit import register
from random import randrange
from re import compile
from threading import Thread, Lock, currentThread
from time import ctime, sleep

class CleanOutPutSet(set):
    def __str__(self):
        return ', '.join(x for x in self)

lock = Lock()
loops = (randrange(2, 5) for x in range(randrange(3, 7)))
remaining  = CleanOutPutSet()
print(loops)

def loop(nsec):
    myname = currentThread().name
    
    #  lock.acquire()
    remaining.add(myname)
    print('%s start at %s'%(myname, ctime()))
    #  lock.release()

    sleep(nsec)

    #  lock.acquire()
    remaining.remove(myname)
    print('%s done  at %s'%(myname, ctime()))
    print('    (remaining : %s)'%(remaining or 'NONE'))
    #  lock.release()

def main():
    for pause in loops:
        Thread(target = loop, args = (pause, )).start()

@register
def _atexit_():
    print('all done at %s'%ctime())

if __name__ == '__main__':
    main()
