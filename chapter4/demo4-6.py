import threading
from time import sleep, ctime

class MyThread(threading.Thread):
    def __init__(self, func, args, name = ''):
        threading.Thread.__init__(self)
        self.func = func
        self.args = args
        self.name = name

    def run(self):
        self.func(*self.args)
        
def loop(loopId, sleepTime):
    print('loop ', loopId, ' start at %s' %ctime())
    sleep(sleepTime)
    print('loop ', loopId, 'done at %s' %ctime())

def main():
    print('start at %s'%ctime())
    threads=[]
    sleepTime = range(5)
    loopId = range(5)

    for i in loopId:
        t = MyThread(loop, (loopId[i], sleepTime[i]), loop.__name__)
        threads.append(t)

    for i in loopId:
        threads[i].start()

    for i in loopId:
        threads[i].join()

    print('all done at %s' %ctime())

if __name__ == '__main__':
    main()

