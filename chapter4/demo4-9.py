from atexit import register
from re import compile
from threading import Thread
from time import ctime
#  from urllib import request.urlopen as uopen
import urllib.request

#  REGEX = compile('#([\d,]+) in Books ')
REGEX = compile(b'(?<=\"p\":\")\d*\.\d*')


JD = 'http://p.3.cn/prices/get?skuid=J_'
PIDs = {'3212154' : 'DDR4 2133 4G',
        '3212616' : 'DDR4 2133 8G',
        '5159060' : 'DDR4 2133 16G'
        }
def getPrice(pid):
    #  print('%s%s'%(JD, pid))
    page = urllib.request.urlopen('%s%s'%(JD, pid))
    data = page.read()
    page.close()
    return REGEX.search(data).group(0).decode()

def _showPrice(pid):
    print(' %r price : %s'%(PIDs[pid], getPrice(pid)))

def main():
    print ('At %s on JD...'%ctime())
    #  for pid in PIDs:
        #  Thread(target = _showPrice, args = (pid, )).start()

    for pid in PIDs:
        _showPrice(pid)

@register
def _atexit_():
    print('all done at %s'%ctime())

if __name__ == '__main__':
    main()


