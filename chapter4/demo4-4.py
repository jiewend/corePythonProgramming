import threading
from time import sleep, ctime


def loop(loopId, sleepTime):
    print('loop ', loopId, ' start at %s' %ctime())
    sleep(sleepTime)
    print('loop ', loopId, 'done at %s' %ctime())

def main():
    print('start at %s'%ctime())
    threads = []
    sleepTime = range(5)
    loopId = range(5)
    for i in loopId:
        t = threading.Thread(target = loop, args = (loopId[i] ,sleepTime[i]))
        threads.append(t)
    
    for i in loopId:
        threads[i].start()

    for i in loopId:
        threads[i].join()

    print('all done at %s'%ctime())

if __name__ == '__main__':
    main()


