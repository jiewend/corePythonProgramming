import re
from threading import Thread
from atexit import register
from sys import argv
from time import ctime


lowerList = [chr(ord('a') + i) for i in range(0, 26)]
capteList = [chr(ord('A') + i) for i in range(0, 26)]

def letterCountInFile(letter, fileName):
    f = open(fileName, 'r')
    count = 0
    for eachLine in f:
        m = re.findall(letter, eachLine)
        count = count + len(m)
    print('%s : %s'%(letter, count))        
    return count

def main():
    print('%s start'%ctime())
    for i in lowerList:
        #  letterCountInFile(i, argv[1])
        Thread(target = letterCountInFile, args = (i, argv[1])).start()

@register
def _atexit_():
    print('%s all done'%ctime())

if __name__ == '__main__':
    main()

