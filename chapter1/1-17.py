import re

f = open('randLogin', 'r')
weekdays = ['Mon', 'Thu', 'Wed', 'Tue', 'Fri', 'Sat', 'Sun']
week = '^(Mon|Thu|Wed|Tue|Fri|Sat|Sun)'
weekdayCount = {}
for eachLine in f:
    m = re.match(week, eachLine)
    day = m.group()
    if day not in weekdayCount:
        weekdayCount[day] = 1
    else:
        weekdayCount[day] += 1
print(weekdayCount)        

