import re

f = open('randLogin', 'r')
Month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
MonthCount = {}
MonthRegex = '(?<=\w{3}\s)\w{3}'
for eachLine in f:
    m = re.search(MonthRegex, eachLine)
    Months = m.group()
    if Months not in MonthCount:
        MonthCount[Months] = 1
    else:
        MonthCount[Months] +=1
print(MonthCount)        
