from socket import *

HOST = '127.0.0.1'
PORT = 8081
BUFSIZ = 1024
ADDR = (HOST, PORT)

def getPort(tcpCliSock):
    print('enter your port...')
    inputPort = int(input('> '))
    if inputPort:
        port = inputPort
        try:
            addr = ('127.0.0.1', port)
            tcpCliSock.connect(addr)
        except:
            getPort(tcpCliSock)
    else:
        addr = ('127.0.0.1', 8080)
        tcpCliSock.connect(addr)
        


tcpCliSock = socket(AF_INET, SOCK_STREAM)
getPort(tcpCliSock)
while True:
    print('say something to server...')
    data = input('> ')
    if not data:
        break
    tcpCliSock.send(data.encode())
    data = tcpCliSock.recv(BUFSIZ)
    if not data:
        break
    print(data.decode())

tcpCliSock.close()    

