import sys
import re
import select
from socket import*
from time import ctime

HOST = '127.0.0.1'
PORT = 9090
BUFSIZ = 1024
ADDR = (HOST, PORT)

clent = socket(AF_INET, SOCK_STREAM)
clent.connect(ADDR)
print('connect to server')
fd = [clent, sys.stdin]
def startChat(fd):
    while True:
        readyInput, readyOutput, readyException = select.select(fd, [], [])
        for inData in readyInput:
            if inData == fd[0]:
                try:
                    data = fd[0].recv(BUFSIZ)
                    print(data)
                    if re.search('bye', data.decode()) is not None:
                        return
                except:
                    print('server error! please check the server')
            else:
                data = input()
                fd[0].send(('%s   [%s]' %(data, ctime())).encode())
                if re.search('bye', data) is not None:
                    return
startChat(fd)
clent.close()            
