from socket import *
from time import ctime
import re
import select
import sys

HOST = '127.0.0.1'
PORT = 9090
BUFSIZ = 1024
ADDR = (HOST, PORT)

server = socket(AF_INET, SOCK_STREAM)
server.bind(ADDR)
server.listen(3)
fd = [server, sys.stdin]
print('waiting for connected...')
clentSock, addr = fd[0].accept()
print('connected from ', addr)
clentSock.send(('hello~~stranger').encode())
fd.append(clentSock)

def startChat(fd):
    while True:
        readInput, readyOutput, readyException = select.select(fd, [], [])
        for inputdata in readInput:
            if inputdata == fd[2]:
                data = fd[2].recv(BUFSIZ)
                print(data)
                if re.search('bye', data.decode()) is not None:
                    return    
            else:
                data = input()
                fd[2].send(('%s   [%s]' %(data, ctime())).encode())
                if re.search('bye', data) is not None:
                    return
startChat(fd)                
clentSock.close()
