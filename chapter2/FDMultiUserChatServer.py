import sys
import select
import re
from socket import *
from time import ctime


def runningServer(fdList):
    while True:
        readable, writeable, error = select.select(fdList, [], [])
        for read in readable:
            if read == fdList[0]:
                addChater(fdList)
            elif read == fdList[1]:
                data = input()
                if data:
                    broadcastNews('from admin   '+data, serverSocket, fdList)
                if re.search('serve over', data) is not None:
                    return
            else:
                try:
                    data = read.recv(BUFSIZ)
                    broadcastNews(data, read, fdList)
                except:
                    read.close()
                    fdList.remove(read)


def addChater(fdList):
    clentSocket, addr = fdList[0].accept()
    fdList.append(clentSocket)
    print('clent %s connected '%fdList.index(clentSocket))
    broadcastNews('hello everyone !', clentSocket, fdList)

def broadcastNews(words, wordsOwer, fdList):
    print('%s from fd[%s]' %(words, fdList.index(wordsOwer)))
    for fd in fdList[2:len(fdList)-1]:
        if fd != wordsOwer:
            try:
                fd.send(words.encode())
            except:
                fd.close()
                fdList.remove(fd)
#  if __name__ == "__name__":                
HOST = '127.0.0.1'
PORT = 9090
BUFSIZ = 1024
ADDR = (HOST, PORT)

serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(ADDR)
serverSocket.listen(5)
print('waiting for connection...')

fdList = [serverSocket, sys.stdin]

runningServer(fdList)
fd[0].close()



