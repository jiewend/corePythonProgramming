from socket import *
from time import ctime
import re

BUFSIZ = 1024
HOST = '127.0.0.1'
PORT = 8080
ADDR = (HOST, PORT)

tcpServSock = socket(AF_INET, SOCK_STREAM)
tcpServSock.bind(ADDR)
tcpServSock.listen()
print('waiting for connection...')
sock, addr = tcpServSock.accept()
print('connected, let\'s talk')

myTrun = False
while True:
    if myTrun:
        something = input('> ')
        if something:
            if re.search('bye', something) is not None:
               sock.send(('%s   [%s]' %(something, ctime())).encode())
               break    
            else: 
                sock.send(('%s   [%s]' %(something, ctime())).encode())
        else:
            pass
    else:
        recvData = sock.recv(BUFSIZ)
        if not recvData:
            pass
        else:
            print(recvData)
            if re.search('bye', recvData.decode()) is not None:
                break
            else:
                pass
    myTrun = not myTrun

tcpServSock.close()



