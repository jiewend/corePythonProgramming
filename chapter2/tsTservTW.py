from twisted.internet import protocol, reactor
from time import ctime

PORT = 8080
class TSSerProtocol(protocol.Protocol):
    def connectionMade(self):
        clnt = self.clnt = self.transport.getPeer().host
        print('connected from ', clnt)

    def dataReceived(self, data):
        print('returning %s' %data)
        self.transport.write(('[%s] %s' %(ctime(), data)).encode())

factory = protocol.Factory()
factory.protocol = TSSerProtocol
print('waiting for connectio~~~')
reactor.listenTCP(PORT, factory)
reactor.run()
