from socket import *
from time import ctime
import re

BUFSIZ = 1024
HOST = '127.0.0.1'
PORT = 8080
ADDR = (HOST, PORT)

tcpClentSock = socket(AF_INET, SOCK_STREAM)
tcpClentSock.connect(ADDR)

myTrun = True
while True:
    if myTrun:
        something = input('> ')
        if something:

            if re.search('bye', something) is None:
                tcpClentSock.send(('%s    [%s]' %(something, ctime())).encode())
            else:
                tcpClentSock.send(('%s    [%s]' %(something, ctime())).encode())
                break
        else:
            pass
    else:
        data = tcpClentSock.recv(BUFSIZ)
        if data:
            print(data)
            if re.search('bye', data.decode()) is not None:
                break
            else:
                pass
        else:
            pass
    myTrun = not myTrun

tcpClentSock.close()
