import threading
from time import sleep, ctime

class ThreadFunc(object):
    def __init__(self, func, args, name = ''):
        self.name = name
        self.func = func
        self.args = args

    def __call__(self):
        self.func(args)

def loop(loopId, sleepTime):
    print('loop ', loopId, ' start at %s' %ctime())
    sleep(sleepTime)
    print('loop ', loopId, 'done at %s' %ctime())

def main():
    print('start at %s'%ctime())
    threads = []
    sleepTime = range(5)
    loopId = range(5)

    for i in loopId:
        t = threading.Thread(target = ThreadFunc(loop, (loopId[i], sleepTime[i])), loop.name)
        thread.append(t)
    
    for i in loopId:
        thread[i].start()

    for i in loopId:
        thread[i].join()
        
    print('all done at %s' %ctime())

if __name__ == '__main__':
    main(0)
